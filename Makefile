CC = gcc
CFLAGS = -g -std=c99 -Wall -Wconversion -Wno-sign-conversion -Werror -pedantic
VALFLAGS = --leak-check=full --track-origins=yes --show-reachable=yes
EXEC = pruebas
ZIPNAME = cola_entrega.zip

build: main.c cola.c cola.h pruebas_alumno.c testing.c testing.h pila.c pila.h
	$(CC) $(CFLAGS) -o $(EXEC) *.c

run: build
	./$(EXEC)

val: build
	valgrind $(VALFLAGS) ./$(EXEC)

clean:
	rm -f *.o $(EXEC) $(ZIPNAME)

zip:
	zip $(ZIPNAME) cola.c pruebas_alumno.c pila.c pila.h Makefile




