#include "pila.h"
#include <stdlib.h>
#include <stdio.h>

#define CAPACIDAD_MINIMA 10
#define FACTOR_REDIMENSION_AGRANDAR 2
#define FACTOR_REDIMENSION_ACHICAR 2
#define FACTOR_DECISION_ACHICAR 4

struct pila {
    void **datos;
    size_t cantidad;
    size_t capacidad;
};

/* *****************************************************************
 *                    FUNCIONES AUXILIARES
 * *****************************************************************/

static bool _pila_esta_llena(pila_t *pila){
    return pila->capacidad == pila->cantidad;
}

static bool _pila_redimensionar(pila_t* pila, size_t capacidad_nueva){
    void* *datos_nuevo = realloc(pila->datos, capacidad_nueva * sizeof(void*));
    if(datos_nuevo == NULL){
        return false;
    }

    pila->datos = datos_nuevo;
    pila->capacidad = capacidad_nueva;
    return  true;
}

static size_t _calcular_capacidad_aumentada(size_t capacidad_actual){
    return capacidad_actual * FACTOR_REDIMENSION_AGRANDAR;
}

static size_t _calcular_capacidad_reducida(size_t capacidad_actual){
    return capacidad_actual / FACTOR_REDIMENSION_ACHICAR;
}

static bool _pila_aumentar_capacidad(pila_t *pila){
    size_t capacidad_aumentada = _calcular_capacidad_aumentada(pila->capacidad);
    return _pila_redimensionar(pila, capacidad_aumentada);

}

static bool _pila_reducir_capacidad(pila_t *pila){
    size_t capacidad_reducida = _calcular_capacidad_reducida(pila->capacidad);
    return _pila_redimensionar(pila, capacidad_reducida);
}

static bool _pila_requiere_reducir_capacidad(pila_t *pila){
    bool criterio_factor_ocupacion = (pila->capacidad / FACTOR_DECISION_ACHICAR) > pila->cantidad;

    size_t capacidad_reducida = _calcular_capacidad_reducida(pila->capacidad);
    bool criterio_capacidad_minima = capacidad_reducida >= CAPACIDAD_MINIMA;

    return criterio_factor_ocupacion && criterio_capacidad_minima;
}


/* *****************************************************************
 *                    PRIMITIVAS DE LA PILA
 * *****************************************************************/

pila_t* pila_crear(){
    pila_t *pila = malloc(sizeof(pila_t));

    if(pila == NULL){
        return NULL;
    }

    pila->datos = malloc(CAPACIDAD_MINIMA * sizeof(void*));
    if(pila->datos == NULL){
        free(pila);
        return NULL;
    }
    pila->capacidad = CAPACIDAD_MINIMA;
    pila->cantidad = 0;
    return pila;
}

void pila_destruir(pila_t *pila){
    free(pila->datos);
    free(pila);
}

bool pila_esta_vacia(const pila_t *pila){
    return pila->cantidad == 0;
}

bool pila_apilar(pila_t *pila, void* valor){
    if(_pila_esta_llena(pila) && !(_pila_aumentar_capacidad(pila))){
        return false;
    }

    pila->datos[pila->cantidad] = valor;
    pila->cantidad +=1;
    return true;
}

void* pila_ver_tope(const pila_t *pila){
    if(pila_esta_vacia(pila)){
        return NULL;
    }
    return pila->datos[pila->cantidad - 1];
}

void* pila_desapilar(pila_t *pila){
    if(pila_esta_vacia(pila)){
        return NULL;
    }

    void* tope = pila_ver_tope(pila);
    pila->cantidad-=1;
    if(_pila_requiere_reducir_capacidad(pila)){
        _pila_reducir_capacidad(pila);
    }
    return tope;
}