#include "cola.h"
#include "testing.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "pila.h"

/* ******************************************************************
 *                   PRUEBAS UNITARIAS ALUMNO
 * *****************************************************************/
void prueba_EncolarNULL(){
    printf("INICIO DE PRUEBAS: ENCOLAR NULL\n");

    cola_t* cola = cola_crear();
    print_test("Prueba crear cola vacia", cola != NULL);
    print_test("Prueba cola esta vacia inicialmente", cola_esta_vacia(cola));
    print_test("Prueba encolar NULL", cola_encolar(cola, NULL));
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba desencolar NULL", cola_desencolar(cola) == NULL);
    print_test("Prueba esta vacia", cola_esta_vacia(cola) == true);
    print_test("Prueba desencolar es NULL", cola_desencolar(cola) == NULL);
    cola_destruir(cola, NULL);
}

void prueba_FuncionamientoCompleto_UnEnteroEnStack(){
    printf("INICIO DE PRUEBAS: FUNCIONAMIENTO COMPLETO CON UN ENTERO EN EL STACK\n");

    cola_t* cola = cola_crear();
    print_test("Prueba crear cola vacia", cola != NULL);
    print_test("Prueba cola esta vacia inicialmente", cola_esta_vacia(cola));
    print_test("Prueba ver primero es NULL", cola_ver_primero(cola) == NULL);
    print_test("Prueba desencolar es NULL", cola_desencolar(cola) == NULL);
    print_test("Prueba desencolar nuevamente es NULL", cola_desencolar(cola) == NULL);
    print_test("Prueba desencolar nuevamente es NULL", cola_desencolar(cola) == NULL);

    int valor = 5;
    print_test("Prueba encolar un elemento en cola vacia", cola_encolar(cola, &valor));
    print_test("Prueba desencolar", cola_desencolar(cola) == &valor);
    print_test("Prueba cola esta vacia", cola_esta_vacia(cola));
    print_test("Prueba ver primero es NULL", cola_ver_primero(cola) == NULL);
    print_test("Prueba desencolar es NULL", cola_desencolar(cola) == NULL);

    cola_destruir(cola, NULL);
}

void prueba_FuncionamientoCompleto_VariosEnterosEnStack(){
    printf("INICIO DE PRUEBAS: FUNCIONAMIENTO COMPLETO CON VARIOS ENTEROS EN EL STACK\n");


    cola_t* cola = cola_crear();
    print_test("Prueba crear cola vacia", cola != NULL);
    print_test("Prueba desencolar es NULL", cola_desencolar(cola) == NULL);
    print_test("Prueba desencolar nuevamente es NULL", cola_desencolar(cola) == NULL);
    print_test("Prueba desencolar nuevamente es NULL", cola_desencolar(cola) == NULL);

    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    int valor4 = 4;
    print_test("Prueba esta vacia", cola_esta_vacia(cola) == true);
    print_test("Prueba encolar 1", cola_encolar(cola, &valor1));
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba ver primero es 1", cola_ver_primero(cola) == &valor1);
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba encolar 2", cola_encolar(cola, &valor2));
    print_test("Prueba ver primero es 1", cola_ver_primero(cola) == &valor1);
    print_test("Prueba encolar 3", cola_encolar(cola, &valor3));
    print_test("Prueba ver primero es 1", cola_ver_primero(cola) == &valor1);
    print_test("Prueba encolar  4", cola_encolar(cola, &valor4));
    print_test("Prueba ver primero es 1", cola_ver_primero(cola) == &valor1);
    print_test("Prueba desencolar 1", cola_desencolar(cola) == &valor1);
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba ver primero es 2", cola_ver_primero(cola) == &valor2);
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba desencolar 2", cola_desencolar(cola) == &valor2);
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba ver primero es 3", cola_ver_primero(cola) == &valor3);
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba desencolar 3", cola_desencolar(cola) == &valor3);
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba ver primero es 4", cola_ver_primero(cola) == &valor4);
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);
    print_test("Prueba desencolar 4", cola_desencolar(cola) == &valor4);
    print_test("Prueba esta vacia", cola_esta_vacia(cola));
    print_test("Prueba ver primero es NULL", cola_ver_primero(cola) == NULL);
    print_test("Prueba desencolar es NULL", cola_desencolar(cola) == NULL);
    print_test("Prueba desencolar es NULL", cola_desencolar(cola) == NULL);

    cola_destruir(cola, NULL);
}

void prueba_DestruirNoVacia_VariosEnterosEnStack(){
    printf("INICIO DE PRUEBAS: DESTRUIR COLA NO VACIA CON ALGUNOS ENTEROS EN EL STACK\n");

    cola_t* cola = cola_crear();
    print_test("Prueba crear cola vacia", cola != NULL);

    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    int valor4 = 4;
    int valor5 = 5;
    int valor6 = 6;
    print_test("Prueba encolar 1", cola_encolar(cola, &valor1));
    print_test("Prueba encolar 2", cola_encolar(cola, &valor2));
    print_test("Prueba encolar 3", cola_encolar(cola, &valor3));
    print_test("Prueba encolar 4", cola_encolar(cola, &valor4));
    print_test("Prueba encolar 5", cola_encolar(cola, &valor5));
    print_test("Prueba encolar 6", cola_encolar(cola, &valor6));
    print_test("Prueba no esta vacia", cola_esta_vacia(cola) == false);


    cola_destruir(cola, NULL);
    print_test("Prueba se destruyo la cola", true);
}

void prueba_DestruirNoVacia_UnEnteroEnHeap(){
    printf("INICIO DE PRUEBAS: DESTRUIR COLA NO VACIA CON SOLO UN ENTERO EN EL HEAP\n");

    cola_t* cola = cola_crear();
    print_test("Prueba crear cola", cola != NULL);

    /* Apilo un entero */
    int* v = malloc(sizeof(int));
    print_test("Prueba encolar un elemento", cola_encolar(cola, v));

    cola_destruir(cola, free);
    print_test("Prueba se destruyó la cola", true);
}

void prueba_DestruirNoVacia_MuchosEnterosEnHeap(){
    printf("INICIO DE PRUEBAS: DESTRUIR COLA NO VACIA CON MUCHOS ENTEROS EN EL HEAP\n");

    const size_t cant_elementos = 1000;

    cola_t* cola = cola_crear();
    print_test("Prueba crear cola", cola != NULL);

    /* Encolo enteros alojados en el heap */
    int** enteros = malloc(cant_elementos * sizeof(int*));
    size_t i;
    for(i = 1; i <= cant_elementos; i++){
        int* valor = malloc(sizeof(int));
        enteros[i - 1] = valor;
        cola_encolar(cola, valor);
    }

    /* Verifico que se hayan encolado todos los valores */
    print_test("Prueba se encolaron en memoria dinamica 1000 enteros", i == cant_elementos + 1);

    /* Desencolo algunos de los enteros encolados */
    // No los desencolo todos para que queden elementos en la cola cuando se
    // la destruya.
    size_t cant_elementos_a_desencolar = cant_elementos / 2;
    size_t j;
    bool desencolados_ok = true;
    for(j = 1; j <= cant_elementos_a_desencolar; j++){
        int* desencolado = cola_desencolar(cola);
        desencolados_ok = desencolado == enteros[j - 1];
        free(desencolado);
    }
    print_test("Prueba se desencolaron 500 enteros", j == 501);
    print_test("Prueba se desencolaron los valores correctos en el orden correcto", desencolados_ok);
    print_test("Prueba no está vacía", cola_esta_vacia(cola) == false);

    free(enteros);
    cola_destruir(cola, free);
    print_test("Prueba se destruyó la cola", true);
}

void pila_destruir_wrapper(void* dato){
    pila_t* pila = dato;
    pila_destruir(pila);
}

void prueba_DestruirNoVacia_MuchasPilasEncoladas(){
    printf("INICIO DE PRUEBAS: DESTRUIR COLA NO VACIA CON MUCHAS PILAS ENCOLADAS\n");

    const size_t cant_elementos = 1000;

    cola_t* cola = cola_crear();
    print_test("Prueba crear cola vacia", cola != NULL);

    /* Encolo pilas */
    pila_t**  pilas = malloc(cant_elementos * sizeof(pila_t*));
    size_t i;
    for(i = 1; i <= cant_elementos; i++){
        pila_t* pila = pila_crear();
        pilas[i - 1] = pila;
        cola_encolar(cola, pila);
    }

    /* Verifico que se hayan encolado todas las pilas */
    print_test("Prueba se encolaron 1000 pilas", i == cant_elementos + 1);

    /* Desencolo algunas de las pilas encoladas */
    // No las desencolo todas para que queden pilas en la cola cuando se
    // la destruya.
    size_t cant_elementos_a_desencolar = cant_elementos / 2;
    size_t j;
    bool desencolados_ok = true;
    for(j = 1; j <= cant_elementos_a_desencolar; j++){
        pila_t* desencolado = cola_desencolar(cola);
        desencolados_ok = desencolado == pilas[j - 1];
        pila_destruir(desencolado);
    }
    print_test("Prueba se desencolaron 500 pilas", j == 501);
    print_test("Prueba se desencolaron los valores correctos en el orden correcto", desencolados_ok);
    print_test("Prueba no está vacía", cola_esta_vacia(cola) == false);

    free(pilas);
    cola_destruir(cola, pila_destruir_wrapper);
    print_test("Prueba destruyo la cola", true);
}

void pruebas_cola_alumno(void){
    prueba_EncolarNULL();
    prueba_FuncionamientoCompleto_UnEnteroEnStack();
    prueba_FuncionamientoCompleto_VariosEnterosEnStack();
    prueba_DestruirNoVacia_VariosEnterosEnStack();
    prueba_DestruirNoVacia_UnEnteroEnHeap();
    prueba_DestruirNoVacia_MuchosEnterosEnHeap();
    prueba_DestruirNoVacia_MuchasPilasEncoladas();
}